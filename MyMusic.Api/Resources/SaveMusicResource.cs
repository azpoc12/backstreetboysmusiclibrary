namespace MyMusic.Api.Resources
{
    public class SaveMusicResource
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Time { get; set; }
        public int ArtistId { get; set; }
    }
}