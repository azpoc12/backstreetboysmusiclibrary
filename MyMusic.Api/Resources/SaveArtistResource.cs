namespace MyMusic.Api.Resources
{
    public class SaveArtistResource
    {
        public string Name { get; set; }
        public int Popularity { get; set; }
    }
}