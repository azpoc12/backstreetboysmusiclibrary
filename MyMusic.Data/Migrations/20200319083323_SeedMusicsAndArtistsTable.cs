﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyMusic.Data.Migrations
{
    public partial class SeedMusicsAndArtistsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder
                .Sql("INSERT INTO Artists (Name,Popularity) Values ('Linkin Park',3)");
            migrationBuilder
                .Sql("INSERT INTO Artists (Name,Popularity) Values ('Iron Maiden',4)");
            migrationBuilder
                .Sql("INSERT INTO Artists (Name,Popularity) Values ('Flogging Molly',5)");
            migrationBuilder
                .Sql("INSERT INTO Artists (Name,Popularity) Values ('Red Hot Chilli Peppers',6)");
                
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('In The End', (SELECT Id FROM Artists WHERE Name = 'Linkin Park'),20,'4:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('Numb', (SELECT Id FROM Artists WHERE Name = 'Linkin Park'),30,'5:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('Breaking The Habit', (SELECT Id FROM Artists WHERE Name = 'Linkin Park'),40,'5:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('Fear of the dark', (SELECT Id FROM Artists WHERE Name = 'Iron Maiden'),50,'7:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('Number of the beast', (SELECT Id FROM Artists WHERE Name = 'Iron Maiden'),30,'4:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('The Trooper', (SELECT Id FROM Artists WHERE Name = 'Iron Maiden'),10,'6:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('What''s left of the flag', (SELECT Id FROM Artists WHERE Name = 'Flogging Molly'),30,'6:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('Drunken Lullabies', (SELECT Id FROM Artists WHERE Name = 'Flogging Molly'),20,'1:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('If I Ever Leave this World Alive', (SELECT Id FROM Artists WHERE Name = 'Flogging Molly'),20,'9:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('Californication', (SELECT Id FROM Artists WHERE Name = 'Red Hot Chilli Peppers'),10,'8:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('Tell Me Baby', (SELECT Id FROM Artists WHERE Name = 'Red Hot Chilli Peppers'),50,'7:30')");
            migrationBuilder
                .Sql("INSERT INTO Musics (Name, ArtistId,Price,Time) Values ('Parallel Universe', (SELECT Id FROM Artists WHERE Name = 'Red Hot Chilli Peppers'),60,'5:30')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder
                .Sql("DELETE FROM Musics");

            migrationBuilder
                .Sql("DELETE FROM Artists");
        }
    }
}