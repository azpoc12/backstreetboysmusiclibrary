using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using MyMusic.Api.Controllers;
using MyMusic.Api.Mapping;
using MyMusic.Api.Resources;
using MyMusic.Core.Models;
using MyMusic.Core.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace MyMusicAPI.Tests
{
    public class MusicsControllerTest
    {
        [Fact]
        public async Task GetAllMusicsTest()
        {
            var _music = new List<Music>() {
            new Music() { Id=5000, Name="Don't GO Break My Heart ",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=4} },
            new Music() { Id=50002, Name="Nobody Else",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=3} },
            new Music() { Id=50003, Name="Breathe",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=2} },
            new Music() { Id=50004, Name="New Love",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=1} },

            new Music() { Id=50005, Name="Passionate",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=5} },
            new Music() { Id=50006, Name="Chances",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=4} },
            new Music() { Id=50007, Name="No Place ",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=3} },
            new Music() { Id=50008, Name="Is It Just ME",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=4} },

            new Music() { Id=50009, Name="Chateau",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=4} },
            new Music() { Id=50018, Name="The Way It Was",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=2} },
            new Music() { Id=50028, Name="Just Like You Like It",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=5} },
            new Music() { Id=50038, Name="OK ",Price=1.29M, Time="3:36", Artist= new Artist{ Id=1, Name="Backstreet Boys",Popularity=4} },

            };

            // configure mapper
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var mapper = config.CreateMapper();

            // Mock IMusicService
            var mockRepo = new Mock<IMusicService>();

            mockRepo.Setup(x => x.GetAllWithArtist()).Returns(Task.FromResult(_music.AsEnumerable()));

            var controller = new MusicsController(mockRepo.Object, mapper);

            var result = await controller.GetAllMusics();
            var musicList = Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal(12, (musicList.Value as IEnumerable<MusicResource>).Count());
        }

        [Fact]
        public async Task GetMusicByIdTest()
        {
            // configure mapper
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var mapper = config.CreateMapper();

            // Mock IMusicService
            var mockRepo = new Mock<IMusicService>();

            mockRepo.Setup(x => x.GetMusicById(It.IsAny<int>())).Returns(Task.FromResult(new Music() { Id = 5000, Name = "Don't GO Break My Heart", Price = 1.29M, Time = "3:36", Artist = new Artist { Id = 1, Name = "Backstreet Boys", Popularity = 4 } }));

            var controller = new MusicsController(mockRepo.Object, mapper);

            var result = await controller.GetMusicById(5000);
            var musicList = Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal("Don't GO Break My Heart", (musicList.Value as MusicResource).Name);
        }

        [Fact]
        public async Task CreateMusicTest()
        {
            // configure mapper
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var mapper = config.CreateMapper();

            // Mock IMusicService
            var mockRepo = new Mock<IMusicService>();

            mockRepo.Setup(x => x.CreateMusic(It.IsAny<Music>())).Returns(Task.FromResult(new Music() { Id = 1, Name = "Creating new rap music", Price = 1.29M, Time = "3:36", Artist = new Artist { Id = 1, Name = "Backstreet Boys", Popularity = 4 } }));
            mockRepo.Setup(x => x.GetMusicById(It.IsAny<int>())).Returns(Task.FromResult(new Music() { Id = 1, Name = "Creating new rap music", Price = 1.29M, Time = "3:36", Artist = new Artist { Id = 1, Name = "Backstreet Boys", Popularity = 4 } }));

            var newMusic = new SaveMusicResource()
            {
                Name = "Creating new rap music",
                Price = 1.29M,
                Time = "3:36",
                ArtistId = 1
            };
            var controller = new MusicsController(mockRepo.Object, mapper);

            var result = await controller.CreateMusic(newMusic);
            var musicList = Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal("Creating new rap music", (musicList.Value as MusicResource).Name);
            Assert.Equal(1, (musicList.Value as MusicResource).Id);
            Assert.Equal("Backstreet Boys", (musicList.Value as MusicResource).Artist.Name);

        }

        [Fact]
        public async Task UpdateMusicTest()
        {
            // configure mapper
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var mapper = config.CreateMapper();

            // Mock IMusicService
            var mockRepo = new Mock<IMusicService>();

            mockRepo.Setup(x => x.UpdateMusic(It.IsAny<Music>(),It.IsAny<Music>())).Returns(Task.FromResult(new Music() { Id = 5000, Name = "Creating new rap music", Price = 1.29M, Time = "3:36", Artist = new Artist { Id = 1, Name = "Backstreet Boys", Popularity = 4 } }));
            mockRepo.Setup(x => x.GetMusicById(It.IsAny<int>())).Returns(Task.FromResult(new Music() { Id = 5000, Name = "Creating new rap music", Price = 1.29M, Time = "3:36", Artist = new Artist { Id = 1, Name = "Backstreet Boys", Popularity = 4 } }));

            var newMusic = new SaveMusicResource()
            {
                Name = "Creating new rap music",
                Price = 1.29M,
                Time = "3:36",
                ArtistId = 1
            };
            var controller = new MusicsController(mockRepo.Object, mapper);

            var result = await controller.UpdateMusic(5000, newMusic);
            var musicList = Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal("Creating new rap music", (musicList.Value as MusicResource).Name);
            Assert.Equal(5000, (musicList.Value as MusicResource).Id);
            Assert.Equal("Backstreet Boys", (musicList.Value as MusicResource).Artist.Name);

        }

        [Fact]
        public async Task DeleteMusic()
        {
            // configure mapper
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var mapper = config.CreateMapper();

            // Mock IMusicService
            var mockRepo = new Mock<IMusicService>();

            mockRepo.Setup(x => x.DeleteMusic(It.IsAny<Music>()));
            mockRepo.Setup(x => x.GetMusicById(It.IsAny<int>())).Returns(Task.FromResult(new Music() { Id = 5000, Name = "Creating new rap music", Price = 1.29M, Time = "3:36", Artist = new Artist { Id = 1, Name = "Backstreet Boys", Popularity = 4 } }));

            var newMusic = new SaveMusicResource()
            {
                Name = "Creating new rap music",
                Price = 1.29M,
                Time = "3:36",
                ArtistId = 1
            };
            var controller = new MusicsController(mockRepo.Object, mapper);
                
            var result = await controller.DeleteMusic(5000);
            var musicList = Assert.IsType<NoContentResult>(result);
        }
    }
}
