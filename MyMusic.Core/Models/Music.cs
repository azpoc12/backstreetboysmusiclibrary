namespace MyMusic.Core.Models
{
    public class Music
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Time { get; set; }
        public int ArtistId { get; set; }
        public Artist Artist { get; set; }
    }
}